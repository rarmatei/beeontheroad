﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BeeOnTheRoad.Startup))]
namespace BeeOnTheRoad
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
